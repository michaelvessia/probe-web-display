var ALL_FEDERATIONS = {};

// given a line, get the fed
function getFed(line) {
  var fedRegex = /\[(.+?)\]/;
  var match = fedRegex.exec(line);
  if(match) {
    return match[1];
  }
}

// break file into array of lines
function getLines(file) {
  return file.split(/\r?\n/);
}

function sanitizeLine(line) {
  return line.replace('\[1;', '').replace('\[0;', '');
}

function getMeet(line) {
  var meetRegex = /http.*/;
  var match = meetRegex.exec(line);
  if (match) {
    return match[0];
  }
}

function addMeetToFed(allFeds, fed, meet) {
  if (fed) {
    if(allFeds[fed]) {
      // The fed has already been seen, add this line's meet to it's list of meets
      if (meet !== null && meet !== undefined) {
        allFeds[fed].meets.push(meet);
        allFeds[fed].meetCount += 1;
      }
    } else {
      // instantiate new array of meets for the fed
      allFeds[fed] = {};
      allFeds[fed].meets = [meet];
      allFeds[fed].name = fed;
      allFeds[fed].meetCount = 1;
    }
  }
}

// Transform collection of feds into a collection of dom elements 
// for selection and append to dom
function populateFedDropdown(federations) {
  Object.keys(federations).map(function(fedName) {
    var fed = federations[fedName];
    var opt = document.createElement('option');
    opt.textContent = fedName + ': ' + fed.meetCount;
    opt.value = fedName;
    document.getElementById('selectFed').appendChild(opt);
  });
}

function fedSelected(fed) {
  var currentFed = ALL_FEDERATIONS[fed];
  var table = document.getElementById('fedTable');
  table.innerHTML = '';
  createFederationDisplay(currentFed);
}

function createFederationDisplay(fed) {
  var table = document.getElementById('fedTable');
  fed.meets.map(function(meet) {
    var tr = document.createElement('tr');
    var td = document.createElement('td');
    //var meetLink = document.createTextNode(meet);
    var meetLink = document.createElement('a');
    meetLink.setAttribute('href', meet);
    meetLink.innerHTML = meet;
    td.appendChild(meetLink);
    tr.appendChild(td);
    table.appendChild(tr);
  });
  document.body.appendChild(table);
}

// TODO: Replace this with actual reading from the output of the probes
function getFileContents() {
  fetch('sample_output.txt')
    .then(function(response) {
      return response.text();
    })
    .then(function(text) {
      var lines = getLines(text).map(line => sanitizeLine(line));
      var federations = lines.reduce(function(allFeds, line) {
        // for each line, get the fed
        if(!line) {
          return allFeds;
        }
        var meet = getMeet(line);
        var fed = getFed(line);
        addMeetToFed(allFeds, fed, meet);
        return allFeds;
    }, {});

      populateFedDropdown(federations);
      ALL_FEDERATIONS = federations;
  return text;
});
}
getFileContents();
